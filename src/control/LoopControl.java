package control;
import model.Loop;
import viewer.Gui;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;



public class LoopControl {
	private Gui gui ;
	private ActionListener list;
	private Loop loop;
	
	public static void main(String[] args){
		new LoopControl();
	}
	class AddComboListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			if(gui.getCombo() == "Loop1"){
				gui.setResults(loop.Loop1(gui.getnLoopField()));
			}
			else if(gui.getCombo() == "Loop2"){
				gui.setResults(loop.Loop2(gui.getnLoopField()));
			}
			else if(gui.getCombo() == "Loop3"){
				gui.setResults(loop.Loop3(gui.getnLoopField()));
			}
			else if(gui.getCombo() == "Loop4"){
				gui.setResults(loop.Loop4(gui.getnLoopField()));
			}

			else{
				gui.setResults("Please,Choose nested loops again!!!");
				
			}
		}
		
	}
	
	public LoopControl(){
		loop = new Loop();
		gui = new Gui();
		gui.setVisible(true);
		gui.setSize(1000 ,300);
		gui.setLocation(400, 300);
		list = new AddComboListener();
		gui.setListenerBt(list);
	}
	

}
