
package viewer;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import java.awt.event.ActionListener;




public class Gui extends JFrame {
	private JPanel Panel1;
	private JPanel Panel2;
	private JTextArea resultsArea;
	private JComboBox<Object> comboBox;
	private JButton Bt;
	private JTextField textf;
	private JLabel label;

	public Gui() {
		// TODO Auto-generated constructor stub
		createFrame();
		createPanel();
		
	}
	public void createFrame(){
		resultsArea = new JTextArea("Choose loops");
		label = new JLabel("Put num loops");
		textf = new JTextField(8);
		comboBox = new JComboBox<>();
		comboBox.addItem("Loop1");
		comboBox.addItem("Loop2");
		comboBox.addItem("Loop3");
		comboBox.addItem("Loop4");
		Bt = new JButton("OK"); 
	
	}
	public void createPanel(){
		Panel1 = new JPanel();
		Panel1.setLayout(new GridLayout(1, 2));
		Panel1.add(resultsArea);
		Panel2 = new JPanel();
		Panel2.add(label);
		Panel2.add(textf);
		Panel2.add(comboBox);
		Panel2.add(Bt);
		Panel1.add(Panel2);
		add(Panel1,BorderLayout.CENTER);
		
	}
	public int getnLoopField(){
		// TODO Auto-generated constructor stub
		String str = textf.getText();
		int n = Integer.parseInt(str);
		return n;
	}
	public void setListenerBt(ActionListener list){
		// TODO Auto-generated constructor stub
		Bt.addActionListener(list);
	}
	public void setResults(String str){
		// TODO Auto-generated constructor stub
		resultsArea.setText(str);
	}
	
	public String getCombo(){
		// TODO Auto-generated constructor stub
		return (String) comboBox.getSelectedItem();
	}



}
