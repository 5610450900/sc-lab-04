package model;

public class Loop {
	public String Loop1(int n){
		String r = "";
		for(int i=1 ;i<=n ;i=i+1){
			for(int j=1 ;j<=4 ;j=j+1){
				r += "*";
			}
			r += "\n";

		}
		return r;
	}
	public String Loop2(int n){
		String r = "";
		for(int i=1; i<=n; i=i+1){
			for(int j=1; j<=i ;j=j+1){
				r += "*";
			}
			r += "\n";

		}
		return r;
	}
	public String Loop3(int n){
		String r = "";
		for(int i=1; i<=n; i=i+1){
			for(int j=1; j<=5 ;j=j+1){
				if(j%2 ==0){
					r += "*";
				}
				else{
					r += "-";
				}
			}
			r += "\n";

		}
		return r;
	}
	public String Loop4(int n){
		String r = "";
		for(int i=1;i<=n;i=i+1){
			for(int j=1;j<=5;j=j+1){
				if(( i + j)%2 ==0){
					r += "*";
				}
				else{
					r += " ";
				}
			}
			r += "\n";;
		}
		return r;
	}
}
